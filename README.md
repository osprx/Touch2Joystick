# Touch2Joystick
Resistive TouchPanel as Joystick/HID Button device.  
Touch Panel fields trigger joystick button actions. 
  
## Hardware Requirements  
 Arduino Leonardo Micro / pro micro + 2 x 7" Resistive TouchPanel  

## Description  
Both touchpanel has 3 defined rows and 4 columns, all in all 24 defined touch fields.  
 Field1 at the top left of the touch panel corresponds to joystick button 1, field2 to joystick button 2 and so on.   
 In this circuit there are 24 buttons, a total of 32 are possible.  
 When you press one of the fields, the corresponding joystick button is pressed.  
  
 ![schematic_diagram](/touch2joystick/schematic_diagram.jpg)
  
## Additional Functions  
Terminal Commands (Arduino IDE-Terminal):  
 'c': //calibrate Panel1  
 'd': //calibrate Panel2  
 'r': //raw Data Panel1  
 's': //raw Data Panel1  
  
  
  
  
***  
  
Author:  
https://gitlab.com/jwunderlich  
Project on GitLab:  
https://gitlab.com/jwunderlich/touch2joystick  
  
Idea based on:  
https://github.com/MichaelDworkin/Arduino-USB-Macro-Keyboard  
  
 
Copyright notice:    
 Parts of the graphic in the schematic illustration were implemented using images from [Sparkfun Electronics](https://www.sparkfun.com/pages/graphics). All rights reserved by [Sparkfun Electronics](https://www.sparkfun.com/pages/graphics).
 



