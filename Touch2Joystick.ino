/*
 ############################################################
 ###  Touch2Joystick                                      ###
 ###  Resistive TouchPanel as Joystick/HID Button device  ###
 ############################################################
 
 Touch Panel fields trigger joystick button actions
 
 Hardware:
 Arduino Leonardo Micro / pro micro + 2 x 7" Resistive TouchPanel

 Author:
 https://gitlab.com/jwunderlich
 Project on GitLab:
 https://gitlab.com/jwunderlich/touch2joystick
 
 Idea based on:
 https://github.com/MichaelDworkin/Arduino-USB-Macro-Keyboard

 How to Use it:
 Both touchpanel has 3 defined rows and 4 columns, all in all 24 defined touch fields.
 Field1 at the top left of the touch panel corresponds to joystick button 1, field2 to joystick button 2 and so on. 
 In this circuit there are 24 buttons, a total of 32 are possible.
 When you press one of the fields, the corresponding joystick button is pressed.

 Additional Functions:
 Terminal Commands (Arduino IDE-Terminal)
 'c': //calibrate Panel1
 'd': //calibrate Panel2
 'r': //raw Data Panel1
 's': //raw Data Panel1
*/

#include <stdlib.h>
#include <stdint.h>
#include <EEPROM.h>
#include "TouchScreen.h"
#include <Keyboard.h>
//https://github.com/MHeironimus/ArduinoJoystickLibrary
#include <Joystick.h>


// ------------------- TouchPanel1 connection assignment ----------------
//Touchpanel 1
#define P1X1 A1
#define P1X2 A3
#define P1Y1 A0
#define P1Y2 A2

// ------------------- TouchPanel2 connection assignment ----------------
//Touchpanel 2
#define P2X1 A6
#define P2X2 A8
#define P2Y1 A9
#define P2Y2 A7

#define PIEZO_PIN 3  // Pin for piezo buzzer

int calibrate_pn1 = 0;
int calibrate_pn2 = 0;
int CalibVal_pn1[4];
int CalibVal_pn2[4];
boolean pn1_pressed = 1;
boolean pn2_pressed = 1;

//LED-Blink
int RXLED = 17;  // The RX LED has a defined Arduino pin
// The TX LED was not so lucky, we'll need to use pre-defined
// macros (TXLED1, TXLED0) to control that.
// (We could use the same macros for the RX LED too -- RXLED1,
//  and RXLED0.)



// ----------------- Configure TouchPanel Fields -----------------

#define rows 3
#define columns 4

//Define Touchpanel1 Fields Array
const String data1[rows][columns] =
{
  { "1", "2", "3", "4" },
  { "5", "6", "7", "8" },
  { "9", "10", "11", "12"},
};

//Define Touchpanel2 Fields Array
const String data2[rows][columns] =
{
  { "13", "14", "15", "16" },
  { "17", "18", "19", "20" },
  { "21", "22", "23", "24"},
};


// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 700 and 1000 ohms across the X plate

//TouchPanel1 (7" Flexprint=Center): 
//Pin1=X1|Pin2=Y2|Pin3=X2|Pin4=Y1 (Top View, left to right)
//Pin1-Pin3: 1000 Ohm, Pin2-Pin4: 220 Ohm 
TouchScreen ts1 = TouchScreen(P1X1, P1Y1, P1X2, P1Y2, 1000); 

//TouchPanel2 (7" Flexprint=Right): 
//Pin1=X1|Pin2=Y2|Pin3=X2|Pin4=Y1 (Top View, left to right)
//Pin1-Pin3:  700 Ohm, Pin2-Pin4: 220 Ohm 
TouchScreen ts2 = TouchScreen(P2X1, P2Y1, P2X2, P2Y2, 700);  


// Create the Joystick
Joystick_ Joystick;


// ---------------- Setup ------------------------------------------

void setup(void)
{
  Serial.begin(115200);
  pinMode(PIEZO_PIN, OUTPUT);

  // Initialize Keyboard Library
  Keyboard.begin();
  // Initialize Joystick Library
  Joystick.begin();

  EEPROM.get( 0 , CalibVal_pn1 );
  if (CalibVal_pn1[0] >= CalibVal_pn1[1]  || CalibVal_pn1[2] >= CalibVal_pn1[3])
  {
    Serial.println("Wrong calibration values Panel1, load default values");
    CalibVal_pn1[0] = 0;
    CalibVal_pn1[1] = 1000;
    CalibVal_pn1[2] = 0;
    CalibVal_pn1[3] = 1000;
  }

  EEPROM.get( 0 , CalibVal_pn2 );
  if (CalibVal_pn2[0] >= CalibVal_pn2[1]  || CalibVal_pn2[2] >= CalibVal_pn2[3])
  {
    Serial.println("Wrong calibration values Panel2, load default values");
    CalibVal_pn2[0] = 0;
    CalibVal_pn2[1] = 1000;
    CalibVal_pn2[2] = 0;
    CalibVal_pn2[3] = 1000;
  }

  //LED-Blink
  pinMode(RXLED, OUTPUT);  // Set RX LED as an output
  // TX LED is set as an output behind the scenes
  //Serial.begin(9600); //This pipes to the serial monitor
  //Serial1.begin(9600); //This is the UART, pipes to sensors attached to board
}



void loop(void) {
  TSPoint p1 = ts1.getPoint(); // a point object holds x y and z coordinates Panel1
  TSPoint p2 = ts2.getPoint(); // a point object holds x y and z coordinates Panel2

  // we have some minimum pressure we consider 'valid'
  //if (p1.z < ts1.pressureThreshhold) pn1_pressed = 1;
  //if (p1.z > ts1.pressureThreshhold && pn1_pressed)   // if pn1_pressed is 0, means no pressing!

  //continuous pressing until release any touch PANEL1
  if (p1.z > ts1.pressureThreshhold)   // if pn1_pressed is 0, means no pressing!
  {
    tone(PIEZO_PIN, 300); // Send 1KHz sound signal...
    int pn1x = map( p1.x, CalibVal_pn1[0], CalibVal_pn1[1], 0, columns);
    int pn1y = map( p1.y, CalibVal_pn1[2], CalibVal_pn1[3], 0, rows);
    if (pn1y < rows && pn1x < columns)
    {
      //tone(PIEZO_PIN, 300); // Send 1KHz sound signal...
      // Serial.println(data1[y][x]);
      //digitalWrite(PIEZO_PIN, !digitalRead(PIEZO_PIN));
      if (!calibrate_pn1)
      {
        //get the Touchpad Array Value
        String outstr = data1[pn1y][pn1x];

        Joystick.setButton(outstr.toInt() - 1, pn1_pressed);
      }
    }
    else Serial.print("row/column position outside of data array panel 1!");
    //pn1_pressed = 0;
    //tone(PIEZO_PIN, 300); // Send 1KHz sound signal...
    delay(150);        // ...for delay time and prevent taste repeating
    noTone(PIEZO_PIN);     // Stop sound...

    switch (calibrate_pn1)             // select function
    {
      case 1:
        calibrate_pn1 = 2;
        CalibVal_pn1[0] = p1.x; // x min
        CalibVal_pn1[2] = p1.y; // y min
        delay(1000);
        Serial.println("bottom right");
        break;
      case 2:
        calibrate_pn1 = 0;
        Serial.println("");
        CalibVal_pn1[1] = p1.x; // x max
        CalibVal_pn1[3] = p1.y; // y max
        delay(1000);
        Serial.println("calibrating panel1 finished");
        EEPROM.put( 0, CalibVal_pn1);
        Serial.println("X Min \tX Max \tY Min \tY Max");
        Serial.print(String(CalibVal_pn1[0]) + "\t");
        Serial.print(String(CalibVal_pn1[1]) + "\t");
        Serial.print(String(CalibVal_pn1[2]) + "\t");
        Serial.println(String(CalibVal_pn1[3]) + "\t");
        delay(1000);
        break;
      case 3:
        Serial.println("measured values panel1:");
        Serial.print("X = "); Serial.print(p1.x);
        Serial.print("\tY = "); Serial.print(p1.y);
        Serial.println("  ");
        Serial.println("calibrate grid values panel1:");
        Serial.print("X = ");
        Serial.print(pn1x);
        Serial.print("\tY = ");
        Serial.println(pn1y);
        Serial.println("  ");
        break;
    }
  }
  //continuous pressing until release any touch PANEL2
  else if (p2.z > ts2.pressureThreshhold)   // if pn2_pressed is 0, means no pressing!
  {
    tone(PIEZO_PIN, 300); // Send 1KHz sound signal...
    int pn2x = map( p2.x, CalibVal_pn2[0], CalibVal_pn2[1], 0, columns);
    int pn2y = map( p2.y, CalibVal_pn2[2], CalibVal_pn2[3], 0, rows);
    if (pn2y < rows && pn2x < columns)
    {
      //tone(PIEZO_PIN, 300); // Send 1KHz sound signal...
      // Serial.println(data1[y][x]);
      //digitalWrite(PIEZO_PIN, !digitalRead(PIEZO_PIN));
      if (!calibrate_pn2)
      {
        //get the Touchpad Array Value
        String outstr = data2[pn2y][pn2x];

        Joystick.setButton(outstr.toInt() - 1, pn2_pressed);
      }
    }
    else Serial.print("row/column position outside of data array panel 2!");

    //pn2_pressed = 0;
    //tone(PIEZO_PIN, 300); // Send 1KHz sound signal...
    delay(150);        // ...for delay time and prevent taste repeating
    noTone(PIEZO_PIN);     // Stop sound...

    switch (calibrate_pn2)             // select function
    {
      case 1:
        calibrate_pn2 = 2;
        CalibVal_pn2[0] = p2.x; // x min
        CalibVal_pn2[2] = p2.y; // y min
        delay(1000);
        Serial.println("bottom right");
        break;
      case 2:
        calibrate_pn2 = 0;
        Serial.println("");
        CalibVal_pn2[1] = p2.x; // x max
        CalibVal_pn2[3] = p2.y; // y max
        delay(1000);
        Serial.println("calibrating panel2 finished");
        EEPROM.put( 0, CalibVal_pn2);
        Serial.println("X Min \tX Max \tY Min \tY Max");
        Serial.print(String(CalibVal_pn2[0]) + "\t");
        Serial.print(String(CalibVal_pn2[1]) + "\t");
        Serial.print(String(CalibVal_pn2[2]) + "\t");
        Serial.println(String(CalibVal_pn2[3]) + "\t");
        delay(1000);
        break;
      case 3:
        Serial.println("measured values panel2:");
        Serial.print("X = "); Serial.print(p2.x);
        Serial.print("\tY = "); Serial.print(p2.y);
        Serial.println("  ");
        Serial.println("calibrate grid values panel2:");
        Serial.print("X = ");
        Serial.print(pn2x);
        Serial.print("\tY = ");
        Serial.println(pn2y);
        Serial.println("  ");
        break;
    }
  }
  else if (!calibrate_pn1 && !calibrate_pn2)
  {
    for (int i = 0; i <= 23; i++) Joystick.setButton(i, 0);
    noTone(PIEZO_PIN);     // Stop sound...
  }



  // Commands via serial terminal
  // character "c"- Calibrate Touchpanel1
  // character "d"- Calibrate Touchpanel2
  // character "r"- Show absolute position values and grid values Touchpanel1
  // character "s"- Show absolute position values and grid values Touchpanel2

  if (Serial.available())     //When input is made
  {
    char  Selection = Serial.read();     //Get entered character
    Serial.flush();
    Serial.println(Selection);     //Show entered character
    switch (Selection)             //select function
    {
      case 'c': //calibrate Panel1
        calibrate_pn1 = 1;
        Serial.println("Calibrate Panel1");
        Serial.println("Press:");
        Serial.println("Panel1 Top Left");
        break;
      
      case 'd': //calibrate Panel2
        calibrate_pn2 = 1;
        Serial.println("Calibrate Panel2");
        Serial.println("Press:");
        Serial.println("Panel2 Top Left");
        break;  
      
      case 'r': //raw Data Panel1
        if (!calibrate_pn1)
        {
          calibrate_pn1 = 3;
          Serial.println("Position values and grid values Panel1");
          Serial.println("back with \"r\"");
        }
        else
        {
          calibrate_pn1 = 0;
          Serial.println("Joystick is activated again");
        }
        break;

      case 's': //raw Data Panel2
        if (!calibrate_pn2)
        {
          calibrate_pn2 = 3;
          Serial.println("Position values and grid values Panel2");
          Serial.println("back with \"s\"");
        }
        else
        {
          calibrate_pn2 = 0;
          Serial.println("Joystick is activated again");
        }
        break;      
    }
  }
}
